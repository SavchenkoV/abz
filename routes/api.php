<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\Api\EpisodeController;
use \App\Http\Controllers\Api\CharacterController;
use \App\Http\Controllers\Api\AuthController;

Route::post('register',  [AuthController::class, 'register']);
Route::post('login',  [AuthController::class, 'login']);


// Protected
Route::middleware(['auth:sanctum'])->group(function() {
    Route::get('episodes',  [EpisodeController::class, 'index']);
    Route::post('logout',  [AuthController::class, 'logout']);

});

Route::get('episodes/{id}',  [EpisodeController::class, 'show']);
Route::get('characters',  [CharacterController::class, 'index']);
Route::get('characters/search/{name}',  [CharacterController::class, 'search']);
Route::get('characters/random',  [CharacterController::class, 'random']);
