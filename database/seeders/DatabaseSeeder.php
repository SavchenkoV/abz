<?php

namespace Database\Seeders;

use App\Models\Quote;
use App\Models\Character;
use App\Models\Episode;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        \App\Models\User::factory(10)->create();

        Episode::factory()->count(30)
            ->has(Character::factory()->count(3), 'characters')
            ->has(Quote::factory()->count(5), 'quotes')
            ->create();

//        $quote = Quote::factory()->make();
//        dd($quote);



//        \App\Models\Episode::factory(30)->create();
//        \App\Models\Character::factory(100)->create();
//        \App\Models\Quote::factory(500)->create();
    }
}
