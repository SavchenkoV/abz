<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharacterEpisodeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('character_episode', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('episode_id')->unsigned()->nullable();
            $table->bigInteger('character_id')->unsigned()->nullable();
            $table->foreign('episode_id')->references('id')->on('episodes');
            $table->foreign('character_id')->references('id')->on('characters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('character_episode');
    }
}

//     Integrity constraint violation: 1452 Cannot add or update a child row: a foreign key constraint fails
//     (`abz_agency`.`character_episode`, CONSTRAINT `character_episode_episode_id_foreign`
//     FOREIGN KEY (`episode_id`)
//     REFERENCES `episodes` (`id`))
//     (SQL: insert into `character_episode` (`character_id`, `episode_id`) values (1, 1), (1, 2))

