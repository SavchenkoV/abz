<?php

namespace Database\Factories;

use App\Models\Episode;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class EpisodeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Episode::class;

    public function definition()
    {
        return [
            'title' => $this->faker->name(),
            'air_date' => $this->faker->dateTimeBetween('-90 days', 'now')->format('Y-m-d H:m:s'),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
