<?php

namespace Database\Factories;

use App\Models\Character;
use App\Models\Episode;
use App\Models\Quote;
use Illuminate\Database\Eloquent\Factories\Factory;

class QuoteFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Quote::class;

    public function definition()
    {
        return [
            'character_id' => Character::get()->random()->id,
            'episode_id' => Episode::get()->random()->id,
            'quote' => $this->faker->realText($maxNbChars = 255, $indexSize = 2),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
