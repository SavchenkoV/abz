<?php

namespace Database\Factories;

use App\Models\Character;
use Illuminate\Database\Eloquent\Factories\Factory;

class CharacterFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */

    protected $model = Character::class;

    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'birthday' => $this->faker->dateTimeBetween('-14600 days', '-7300 days')->format('Y-m-d H:m:s'),
            'occupations' => json_encode([
                'company' => $this->faker->company,
                'jobTitle' => $this->faker->jobTitle,
            ]),
            'img' => $this->faker->image('public/storage/images',640,480, null, false),
            'nickname' => $this->faker->firstName(),
            'portrayed' => $this->faker->image('public/storage/avatars',128,128, null, false),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
