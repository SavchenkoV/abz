<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EpisodeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'title' => $this->title,
          'air_date' => $this->air_date,
          'characters' => $this->characters
//          'characters' => CharacterResource::collection($this->whenLoaded('characters')) TODO bag when return CharacterResource return on front Null

        ];
    }
}
